package pedestrian_crossing_car_to_x

import kotlinx.coroutines.runBlocking
import org.scenariotools.smlk.CompositeScenarioProgram
import org.scenariotools.smlk4sumo.controllersinterface.*
import org.scenariotools.smlk4sumo.sumo.createSumoScenarioProgram
import pedestrian_crossing_car_to_x.scenarios.*


// testing
fun main(){

    val compositeScenarioProgram = CompositeScenarioProgram()


    val scenarioProgramSumoLayer = createSumoScenarioProgram(
        "sumo-gui.exe",
        "src/test/resources/pedestrian_crossing_car_to_x/data/run.sumocfg",
        0.1
    )

    val v2xControllersRoot = V2XControllersRoot()

    val scenarioProgramV2XControllerInterfaceLayer = createControllerInterfaceScenarioProgram(v2xControllersRoot)

    scenarioProgramV2XControllerInterfaceLayer.activeGuaranteeScenarios.add(triggerPedestriansGreenUponPedestrianApproaching)
    scenarioProgramV2XControllerInterfaceLayer.activeGuaranteeScenarios.add(pedestriansMustHaveGreenFor15Sec)
    scenarioProgramV2XControllerInterfaceLayer.activeGuaranteeScenarios.add(pedestriansGreenMustOccurAfterPedestrianApproaching)


    scenarioProgramV2XControllerInterfaceLayer.activeGuaranteeScenarios.add(vehicleNotifiesTLControllerOnApproach)
    scenarioProgramV2XControllerInterfaceLayer.activeGuaranteeScenarios.add(triggerVehiclesGreenUponVehicleRequest)
    scenarioProgramV2XControllerInterfaceLayer.activeGuaranteeScenarios.add(vehiclesGreenMustOccurAfterVehicleRequest)
    scenarioProgramV2XControllerInterfaceLayer.activeGuaranteeScenarios.add(vehiclesMustHaveGreenFor15Sec)

    scenarioProgramV2XControllerInterfaceLayer.activeGuaranteeScenarios.add(visualizeVehicleApproachingEvent)
    scenarioProgramV2XControllerInterfaceLayer.activeGuaranteeScenarios.add(visualizeTimeKeepingGreenForVehicleEvent)
    scenarioProgramV2XControllerInterfaceLayer.activeGuaranteeScenarios.add(visualizeVehicleControllerInputEvents)

    scenarioProgramV2XControllerInterfaceLayer.activeGuaranteeScenarios.add(tlPhaseChangedScenario(v2xControllersRoot))
    scenarioProgramV2XControllerInterfaceLayer.activeGuaranteeScenarios.add(tlControllerDetectsPedestrianScenario(v2xControllersRoot))
    scenarioProgramV2XControllerInterfaceLayer.activeGuaranteeScenarios.add(manageTLPhaseTransitioning)
    scenarioProgramV2XControllerInterfaceLayer.activeGuaranteeScenarios.add(countTLPhaseActiveDuration)



    compositeScenarioProgram.addScenarioProgram(
        scenarioProgramSumoLayer,
        scenarioProgramV2XControllerInterfaceLayer
    )


    scenarioProgramV2XControllerInterfaceLayer.eventNode.registerSender(scenarioProgramSumoLayer.eventNode, sumoLayerOutputSymbolicEvents)
    scenarioProgramV2XControllerInterfaceLayer.terminatingEvents.add(sumoLayerTerminatingEvents)
    scenarioProgramSumoLayer.eventNode.registerSender(scenarioProgramV2XControllerInterfaceLayer.eventNode, sumoLayerInputSymbolicEvents)


    runBlocking {
        compositeScenarioProgram.run()
    }

}