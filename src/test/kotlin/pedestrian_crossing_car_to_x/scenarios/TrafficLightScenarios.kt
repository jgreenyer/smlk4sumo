package pedestrian_crossing_car_to_x.scenarios

import org.scenariotools.smlk.*
import org.scenariotools.smlk4sumo.controllersinterface.TrafficLightController
import org.scenariotools.smlk4sumo.controllersinterface.V2XControllersRoot
import org.scenariotools.smlk4sumo.sumo.*


fun tlPhaseChangedScenario(v2xControllersRoot: V2XControllersRoot) = scenario(
    SumoTrafficLight::getPhase.symbolicEvent()
)
{
    val sumoTrafficLight = it.receiver
    val phase1 = it.result()!!
    val phase2 = waitFor(sumoTrafficLight.getPhase()).result()!!

    if (phase1 != phase2) {
        val trafficLightController = v2xControllersRoot.trafficLightControllers[sumoTrafficLight.id]!!
        urgent(trafficLightController.phaseChanged(phase2))
    }
}


fun tlControllerDetectsPedestrianScenario(v2xControllersRoot: V2XControllersRoot) = scenario(
    SumoPerson::changedEdgeTo.symbolicEvent()
)
{

    val newEdge = it.parameters[0] as SumoEdge
    val previousEdge = it.receiver.previousEdge

    previousEdge?.let {
        fun getTrafficLightAssociatedWith(edge: SumoEdge): TrafficLightController? {
            return if (edge.id.startsWith(":")) {
                val trafficLightID = edge.id.substringBefore("_").removePrefix(":")
                v2xControllersRoot.trafficLightControllers.get(trafficLightID)
            } else
                null
        }


        getTrafficLightAssociatedWith(newEdge)?.let {
            if (getTrafficLightAssociatedWith(previousEdge) == null) {
                //println("Yea!! pedestrianApproachingCrossing")
                urgent(it.pedestrianApproachingCrossing())
            }
        }

        getTrafficLightAssociatedWith(previousEdge)?.let {
            if (getTrafficLightAssociatedWith(newEdge) == null) {
                urgent(it.pedestrianLeftCrossing())
                if (it.pedestrianCounter == 0)
                    urgent(it.allPedestrianLeftCrossing())
            }
        }
    }
}


val manageTLPhaseTransitioning = scenario(
    TrafficLightController::turn.symbolicEvent()
)
{

    val phaseToSet = it.parameters[0] as Int
    val trafficLightController = it.receiver

    //println("~~~ requesting to transition TL phase to ${phaseToSet}...")

    if (trafficLightController.currentPhase != phaseToSet) {

        forbiddenEvents.add(trafficLightController receives TrafficLightController::turn)

        println("~~~ current TL phase is ${trafficLightController.currentPhase}...")
        println("~~~ requested to transition to $phaseToSet, now waiting until phase occurs, and blocking further transitionToPhase events until then. ")

        if (phaseToSet == TrafficLightPhase.pedestriansGreen) {
            assert(trafficLightController.currentPhase == TrafficLightPhase.vehicleGreen) // this scenario only works if there is no automatic TL timing of SUMO interfering, all tlLogic phase duration must be set to high values
            request(trafficLightController.trafficLight.setPhase(TrafficLightPhase.vehicleYellow))
            waitFor(trafficLightController.phaseChanged(TrafficLightPhase.vehicleYellow))

            waitForSimulationSteps(40)

            urgent(trafficLightController.trafficLight.setPhase(TrafficLightPhase.pedestriansGreen))
            waitFor(trafficLightController.phaseChanged(TrafficLightPhase.pedestriansGreen))
        }

        if (phaseToSet == TrafficLightPhase.vehicleGreen) {
            assert(trafficLightController.currentPhase == TrafficLightPhase.pedestriansGreen) // this scenario only works if there is no automatic TL timing of SUMO interfering, all tlLogic phase duration must be set to high values
            request(trafficLightController.trafficLight.setPhase(TrafficLightPhase.vehicleAndPedestriansRed))
            waitFor(trafficLightController.phaseChanged(TrafficLightPhase.vehicleAndPedestriansRed))

            waitForSimulationSteps(80)

            request(trafficLightController.trafficLight.setPhase(TrafficLightPhase.vehicleGreen))
            waitFor(trafficLightController.phaseChanged(TrafficLightPhase.vehicleGreen))
        }

        println("~~~ OCCURRED  $phaseToSet, not blocking further transitionToPhase events anymore.")
    }
}

val countTLPhaseActiveDuration = scenario(
    TrafficLightController::phaseChanged.symbolicEvent()
)
{


    val trafficLightController = it.receiver
    //val phase = trafficLightController.currentPhase

    interruptingEvents.add(trafficLightController receives TrafficLightController::phaseChanged)

    var sec = 0
    while (true) {
        waitForSimulationSteps(10)
        sec++
        urgent(trafficLightController.currentPhasedIsActiveFor(sec))
        //println("§§§§§§ phase $phase is active for $sec seconds...")
    }

}