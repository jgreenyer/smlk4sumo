package org.scenariotools.smlk4sumo.controllersinterface

import de.tudresden.ws.container.SumoPosition2D
import org.scenariotools.smlk.event

class V2XControllersRoot {

    val vehicleControllers: MutableMap<String, VehicleController> = mutableMapOf()
    fun addVehicleController(id: String) = event(id) {
        val vehicleController = VehicleController(id)
        vehicleControllers[id] = vehicleController
        vehicleController
    }

    fun removeVehicleController(id: String) = event(id) {
        vehicleControllers.remove(id)
    }

    val trafficLightControllers: MutableMap<String, TrafficLightController> = mutableMapOf()
    fun addTrafficLightController(id: String, xPos : Double, yPos: Double) = event(id, xPos, yPos) {
        val trafficLightController = TrafficLightController(id, xPos = xPos, yPos = yPos)
        trafficLightControllers[id] = trafficLightController
        trafficLightController
    }

}


class VehicleController(val id : String, var laneID: String? = null, var positionX : Double = 0.0, var positionY : Double = 0.0, var speed : Double = 0.0){
    fun speedChanged(speed : Double) = event(speed){this.speed = speed}
    fun positionChanged(oldPosition : SumoPosition2D, newPosition : SumoPosition2D) = event(oldPosition, newPosition){
        this.positionX = newPosition.x
        this.positionY = newPosition.y
    }
    fun laneChanged(laneID : String) = event(laneID){this.laneID = laneID}

    fun enteredOuterTLPerimeter(trafficLightController : TrafficLightController) = event(trafficLightController){   }
    fun exitedOuterTLPerimeter(trafficLightController : TrafficLightController) = event(trafficLightController){   }
    fun enteredInnerTLPerimeter(trafficLightController : TrafficLightController) = event(trafficLightController){   }
    fun exitedInnerTLPerimeter(trafficLightController : TrafficLightController) = event(trafficLightController){   }

    fun timeKeepingGreen(trafficLightController : TrafficLightController, sec : Int) = event(trafficLightController, sec){   }

    override fun toString() = "VehicleController(id=$id)"
}

class TrafficLightController(val id : String, var currentPhase : Int = 0, val xPos : Double, val yPos: Double){

    val trafficLight = TrafficLight(id)

    fun phaseChanged(phase : Int) = event(phase) {this.currentPhase = phase}

    fun vehicleApproaching(vehicleController: VehicleController) = event(vehicleController) {}

    var pedestrianCounter = 0

    fun pedestrianApproachingCrossing() = event {pedestrianCounter++}

    fun pedestrianLeftCrossing() = event {pedestrianCounter--}

    fun allPedestrianLeftCrossing() = event{}

    fun turn(tlPhase : Int) = event(tlPhase){}

    var currentPhaseActiveSeconds = 0
    fun currentPhasedIsActiveFor(sec : Int) = event(sec){currentPhaseActiveSeconds = sec}


    override fun toString() = "TrafficLightController(id=$id)"
}

class TrafficLight(val id : String){
    fun setPhase(tlPhase : Int) = event (tlPhase) {}
}

object Visualization{
    fun showArrow(id : String, labelText : String, x1 : Double, y1 : Double, x2 : Double, y2 : Double, rgbaRED:Int=255, rgbaGREEN:Int=255, rgbaBLUE:Int=0, rgbaAlpha:Int=255, stepsDuration : Int) = event(id, labelText, x1, y1, x2, y2, rgbaRED, rgbaGREEN, rgbaBLUE, rgbaAlpha, stepsDuration){}
}

object TrafficLightPhase{
    const val vehicleGreen = 0
    const val vehicleYellow = 1
    const val pedestriansGreen = 2
    const val vehicleAndPedestriansRed = 3
}
