package org.scenariotools.smlk4sumo.controllersinterface

import org.scenariotools.smlk.*
import org.scenariotools.smlk4sumo.controllersinterface.scenarios.*
import org.scenariotools.smlk4sumo.sumo.SumoPerson
import org.scenariotools.smlk4sumo.sumo.SumoSimulation
import org.scenariotools.smlk4sumo.sumo.SumoTrafficLight
import org.scenariotools.smlk4sumo.sumo.SumoVehicle
import kotlin.reflect.KFunction

fun createControllerInterfaceScenarioProgram(v2xControllersRoot : V2XControllersRoot) : ScenarioProgram {

    val scenarioProgram = ScenarioProgram("V2XControllerInterfaceLayer", terminatingEvents = mutableSetOf(SumoSimulation::close.symbolicEvent()))


    scenarioProgram.activeGuaranteeScenarios.add(vehicleAppearedScenario(v2xControllersRoot))
    scenarioProgram.activeGuaranteeScenarios.add(vehicleDisappearedScenario(v2xControllersRoot))
    scenarioProgram.activeGuaranteeScenarios.add(vehicleSpeedChangedScenario(v2xControllersRoot))
    scenarioProgram.activeGuaranteeScenarios.add(vehiclePositionChangedScenario(v2xControllersRoot))
    scenarioProgram.activeGuaranteeScenarios.add(vehicleLaneChangedScenario(v2xControllersRoot))
    scenarioProgram.activeGuaranteeScenarios.add(vehiclePassingTLScenario(v2xControllersRoot))

    scenarioProgram.activeGuaranteeScenarios.add(tlControllerCreationScenario(v2xControllersRoot))


    scenarioProgram.addEnvironmentMessageType(*sumoLayerOutputEventTypes)

    return scenarioProgram
}

val sumoLayerOutputEventTypes = arrayOf(
    SumoSimulation::start,
    SumoSimulation::timestep,
    SumoSimulation::close,
    SumoSimulation::vehicleAppeared,
    SumoSimulation::vehicleDisAppeared,
    SumoSimulation::personAppeared,
    SumoSimulation::personDisAppeared,
    SumoVehicle::getLane,
    SumoVehicle::getPosition,
    SumoVehicle::getSpeed,
    SumoPerson::changedEdgeTo,
    SumoTrafficLight::getPhase
) as Array<KFunction<ObjectEvent<Any,Any?>>>
val sumoLayerOutputSymbolicEvents = setOf(*sumoLayerOutputEventTypes).symbolicEvents()

val sumoLayerTerminatingEvents = SumoSimulation::close.symbolicEvent()

val sumoLayerInputEventTypes = arrayOf(
    TrafficLight::setPhase,
    Visualization::showArrow
) as Array<KFunction<ObjectEvent<Any,Any?>>>
val sumoLayerInputSymbolicEvents = setOf(*sumoLayerInputEventTypes).symbolicEvents()


