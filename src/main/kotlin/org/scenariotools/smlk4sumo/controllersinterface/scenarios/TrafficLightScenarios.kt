package org.scenariotools.smlk4sumo.controllersinterface.scenarios

import org.scenariotools.smlk.scenario
import org.scenariotools.smlk.symbolicEvent
import org.scenariotools.smlk4sumo.controllersinterface.V2XControllersRoot
import org.scenariotools.smlk4sumo.sumo.SumoSimulation
import org.scenariotools.smlk4sumo.sumo.urgent

fun tlControllerCreationScenario(v2xControllersRoot: V2XControllersRoot) = scenario(
    SumoSimulation::start.symbolicEvent()
)
{
    val sumoSimulation = it.receiver
    for (sumoTrafficLight in sumoSimulation.trafficLights.values) {
        val (x, y) = sumoTrafficLight.getCoordinates(sumoSimulation)
        urgent(v2xControllersRoot.addTrafficLightController(sumoTrafficLight.id, x, y))
    }
}