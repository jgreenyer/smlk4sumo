package org.scenariotools.smlk4sumo.sumo

import it.polito.appeal.traci.SumoTraciConnection
import org.scenariotools.smlk.ScenarioProgram
import org.scenariotools.smlk4sumo.controllersinterface.TrafficLight
import org.scenariotools.smlk4sumo.controllersinterface.Visualization
import org.scenariotools.smlk4sumo.sumo.scenarios.*

fun createSumoScenarioProgram(sumo_bin:String, config_file:String, step_length_sec:Double) : ScenarioProgram{

    val sumoTraciConnection = SumoTraciConnection(sumo_bin, config_file)
    sumoTraciConnection.addOption("step-length", step_length_sec.toString())
    val sumoSimulation = SumoSimulation(sumoTraciConnection)

    val scenarioProgram = ScenarioProgram("SumoLayer")

    scenarioProgram.activeAssumptionScenarios.add{
        request(sumoSimulation.start())
        do {
            request(sumoSimulation.timestep())
        } while (sumoSimulation.getMinExpectedNumber() > 0)
        request(sumoSimulation.close())
    }

    scenarioProgram.addEnvironmentMessageType(
        SumoSimulation::start,
        SumoSimulation::timestep,
        SumoSimulation::close,
        TrafficLight::setPhase,
        Visualization::showArrow
    )


    scenarioProgram.activeGuaranteeScenarios.add(vehiclesAppearanceDisappearanceDetector(sumoSimulation))
    scenarioProgram.activeGuaranteeScenarios.add(vehiclesPositionAndSpeedMaintainer(sumoSimulation))

    scenarioProgram.activeGuaranteeScenarios.add(trafficLightPhaseMonitor(sumoSimulation))
    scenarioProgram.activeGuaranteeScenarios.add(trafficLightPhaseChange(sumoSimulation))

    scenarioProgram.activeGuaranteeScenarios.add(personsMaintainer(sumoSimulation))
    scenarioProgram.activeGuaranteeScenarios.add(personsEdgeMaintainer(sumoSimulation))

    scenarioProgram.activeGuaranteeScenarios.add(showArrow(sumoSimulation))

    return scenarioProgram
}
