package org.scenariotools.smlk4sumo.sumo.scenarios

import org.scenariotools.smlk.scenario
import org.scenariotools.smlk.symbolicEvent
import org.scenariotools.smlk4sumo.controllersinterface.TrafficLight
import org.scenariotools.smlk4sumo.sumo.SumoSimulation
import org.scenariotools.smlk4sumo.sumo.urgent

fun trafficLightPhaseMonitor(sumoSimulation: SumoSimulation) = scenario {
    do {
        waitFor(sumoSimulation.timestep())

        for (sumoTrafficLight in sumoSimulation.trafficLights.values) {
            urgent(sumoTrafficLight.getPhase())
        }

    } while (!sumoSimulation.closed)
}

fun trafficLightPhaseChange(sumoSimulation: SumoSimulation) = scenario(TrafficLight::setPhase.symbolicEvent()) {
    val trafficLight = it.receiver
    val phaseToSet = it.parameters[0] as Int
    val sumoTrafficLight = sumoSimulation.trafficLights[trafficLight.id]!!
    urgent(sumoTrafficLight.setPhase(phaseToSet))
}

