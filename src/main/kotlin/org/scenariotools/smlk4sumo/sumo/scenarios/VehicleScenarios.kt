package org.scenariotools.smlk4sumo.sumo.scenarios

import org.scenariotools.smlk.scenario
import org.scenariotools.smlk4sumo.sumo.SumoSimulation
import org.scenariotools.smlk4sumo.sumo.urgent

fun vehiclesAppearanceDisappearanceDetector(sumoSimulation: SumoSimulation) = scenario {
    do {
        waitFor(sumoSimulation.timestep())

        // Note: "departed" are vehicles that started their route; "arrived" are those that arrived at the end of their route
        for (id in sumoSimulation.getDepartedIDList()) {
            urgent(sumoSimulation.vehicleAppeared(id))
        }
        for (id in sumoSimulation.getArrivedIDList()) {
            urgent(sumoSimulation.vehicleDisAppeared(id))
        }
        urgent(sumoSimulation.vehicleListUpdated())
    } while (!sumoSimulation.closed)
}

fun vehiclesPositionAndSpeedMaintainer(sumoSimulation: SumoSimulation) = scenario {
    do {
        waitFor(sumoSimulation.vehicleListUpdated())
        for (sumoVehicle in sumoSimulation.vehicles.values) {
            urgent(sumoVehicle.getLane())
            urgent(sumoVehicle.getPosition())
            urgent(sumoVehicle.getSpeed())
        }
    } while (!sumoSimulation.closed)
}
