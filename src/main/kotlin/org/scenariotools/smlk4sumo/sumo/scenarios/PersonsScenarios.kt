package org.scenariotools.smlk4sumo.sumo.scenarios

import org.scenariotools.smlk.scenario
import org.scenariotools.smlk4sumo.sumo.SumoSimulation
import org.scenariotools.smlk4sumo.sumo.urgent

fun personsMaintainer(sumoSimulation: SumoSimulation) = scenario {

    val personsIDList = mutableSetOf<String>()
    do {
        waitFor(sumoSimulation.timestep())
        val newPersonsIDList = sumoSimulation.getPersonsIDList()

        val appearedPersonsIDList = newPersonsIDList.toMutableSet().also { it.removeAll(personsIDList) }
        for (id in appearedPersonsIDList) {
            urgent(sumoSimulation.personAppeared(id))
        }

        val disappearedPersonsIDList = personsIDList.toMutableSet().also { it.removeAll(newPersonsIDList) }
        for (id in disappearedPersonsIDList) {
            urgent(sumoSimulation.personDisAppeared(id))
        }

        personsIDList.clear()
        personsIDList.addAll(newPersonsIDList)

        urgent(sumoSimulation.personListUpdated())

    } while (!sumoSimulation.closed)
}

fun personsEdgeMaintainer(sumoSimulation: SumoSimulation) = scenario {
    do {

        waitFor(sumoSimulation.personListUpdated())

        //for each edge check the persons on it, then check whether the edge has changed.
        for (sumoEdge in sumoSimulation.edges.values.toTypedArray()) {
            for (personID in sumoEdge.getLastStepPersonIDs()) {
                val sumoPerson = sumoSimulation.persons.get(personID)!!
                if (sumoEdge != sumoPerson.edge) {
                    urgent(sumoPerson.changedEdgeTo(sumoEdge))
                }
            }
        }

    } while (!sumoSimulation.closed)
}
